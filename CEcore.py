#!/usr/bin/env python3

import numpy as np
import itertools
import pdb
import tensorflow as tf
from scipy import optimize
from joblib import Parallel, delayed


tf.enable_eager_execution()  # used for sparse stuff


########################################################################
# INTERNAL HELPER FUNCTIONS
########################################################################
def __numpify(*args):
    """
    Converts lists of chords or probabilities to 2D numpy arrays.
    """

    assert len(args) > 0, 'Must specify at least one list to numpify!'

    # iterate through arguments, parse, add to out list
    out = []
    for arg in args:
        if arg is None:
            out += [None]
            continue
        try:
            # convert to a numpy array of floats of min dimension 2.
            # if the chords are different sizes, this throws ValueError since
            # numpy can't convert e.g. [[1,2,3], [4,5]] to an ndarray of floats
            out += [np.array(arg, ndmin=2, dtype=float)]

            # weird case - if the user passed a list of numbers without
            # enclosing in a double list, like:
            #     [0, 200, 400, 500, 700, 900, 1100] -
            # the above will put that in a 2D ndarray, which looks like:
            #     np.array([[0, 200, 400, 500, 700, 900, 1100]])
            # but the way we parse it, this looks like a 7-note chord. we want:
            #     np.array([[0], [200], [400], [500], [700], [900], [1100]])
            # which is the transpose of the above.
            # For this, we'd check the first entry of the input argument and
            # see if it's a number - except we don't know what type the user
            # input (int? float? some numpy data type?). So we just check to
            # see if the first entry isn't a list or an ndarray
            if type(arg[0]) != list and type(arg[0]) != np.ndarray:
                out[-1] = out[-1].T
        except ValueError as e:
            if e.args[0] == "setting an array element with a sequence.":
                print("ERROR: chords need to all be the same size!")
            else:
                raise e

    if len(args) > 1:
        return tuple(out)  # slightly nicer if you return tuple instead of list
    else:
        return out[0]      # only one argument, no need to return a tuple


def __entropy(p, a):
    """
    Returns the Renyi entropy of order a of the probability distribution `p`
    (in bits).

    `a` is a real number >= 1, or the special value numpy.inf.

    `p` is given by a numpy array of probabilities, assumed to sum to 1.
    """
    if 'tensorflow' in str(type(p)):
        p = p.values.numpy()
    else:
        p = p[p > 0]    # get rid of entries with 0% probability

    if a == 1:
        return -(p*np.log2(p)).sum()
    elif a == np.inf:
        return -np.log2(np.max(p))
    else:
        return 1/(1-a)*np.log2((p**a).sum())


def __wrapcentered(v, equiv=1200):
    """
    This wraps the numpy array `v` at the interval of equivalence (set to 1200
    cents by default).

    Note this is centered at 0, so that for example, if `equiv`=1200, 601 cents
    becomes -599 cents, 800 cents becomes -400 cents, etc.
    """

    # OLD return np.abs(equiv/2-((v-equiv/2) % equiv))
    return ((v-equiv/2) % equiv)-equiv/2


def __wrap(v, equiv=1200):
    """
    This wraps the numpy array `v` at the interval of equivalence (set to 1200
    cents by default).
    """

    # OLD return np.abs(equiv/2-((v-equiv/2) % equiv))
    return v % equiv


def __wrapnormalpdf(v, s, transpose=False, equiv=1200):
    """
    This is a multivariate wrapped normal distribution, wrapped at `equiv` (set
    to 1200 cents by default), with `s` the standard deviation.

    The parameter `transpose` indicates whether we're using transpositionally-
    invariant CE or not. If we are, then we generate the appropriate covariance
    matrix.

    Rather than using the Jacobi theta function, we just wrap the vector around
    `equiv` and reflect at `equiv`. Since the Gaussian decays so quickly, this
    approximation is good unless `s` is very high.

    If the equivalence interval is an octave, this approximation should be good
    to at least 7 decimal places as long as `s` < 100 or so.

    `v` should be a numpy 2D array representing a list of vectors, in which the
    first axis specifies the vector and the second represents the different
    coordinates of the vector. If you're only getting one vector, the array
    should still have two axes.

    This function returns a 1d numpy array with the results.
    """

    # note that if this is transposed, "chordsize" below is one less than the
    # "n" in n-CE (so that 3-CE has chordsize=2, etc)
    chordsize = np.shape(v)[1]

    # we know the determinant and inverse are in easy closed forms
    if transpose:
        # scale s if transposed, so that it always refers to "total" s.
        # (probably more elegant way to do this, but whatever)
        s = s / ((chordsize+1)**(1/(2*chordsize)))
        det = s**(2*chordsize)*(chordsize+1)
        inv = (np.eye(chordsize)
               - np.ones((chordsize, chordsize))/(chordsize+1))/s**2
    else:
        det = s**(2*chordsize)
        inv = 1/s**2    # we don't need np.eye, scalar will do

    v = __wrapcentered(v, equiv)

    # note - the (v.dot(inv)*v).sum(axis=1) is a vectorized way to do a bunch
    # of quadratic form products at the same time
    return (1/np.sqrt((2*np.pi)**chordsize * det)
            * np.exp(-1/2 * (v.dot(inv)*v).sum(axis=1)))


def __sparsewrapnormalpdf(s, n, incr, tol=1e-10, transpose=False, equiv=1200):
    """
    Generates a sparse n-dimensional wrapped normal pdf. Returns a flattened
    sparse scipy vector for the pdf.

    `s` gives the standard deviation per note.
    `n` is the dimensionality.
    `incr` gives the n-dimensional cent grid increment.
    `tol` is the tolerance - if the quotient of the value of the Gaussian,
      divided by the value at 0, is less than tol, quantize it to 0.
    `transpose` gives whether we are using the transposition-invariant Gaussian
       or the white one.
    """

    # check if we've already computed this
    if __sparsewrapnormalpdf.computed.get((s, n, incr, tol, transpose, equiv)):
        return __sparsewrapnormalpdf.computed.get((s, n, incr, tol, transpose,
                                                   equiv))

    # first get bounding box - this is an analytic expression that can be
    # obtained by dividing the Gaussian by the value at 0, and solving for tol
    if transpose:
        ##@ FIXME CHANGE THIS FOR TOTAL S!!
        cutoff = np.sqrt(-4 * s**2 * np.log(tol))
    else:
        cutoff = np.sqrt(-2 * s**2 * np.log(tol))
    cutoff = np.ceil(cutoff/incr) * incr

    # create giant sparse vector, done in python as a tuple of (inds, vals)
    inds = np.array([v for v in itertools.product(
                                    np.r_[-cutoff:(cutoff+incr):incr],
                                    repeat=n)])

    vals = __wrapnormalpdf(inds, s, transpose, equiv)
    vals = vals/vals.sum()
    inds = __wrap(inds, equiv)
    __sparsewrapnormalpdf.computed[(s, n, incr, tol, transpose, equiv)] \
        = {"inds": inds, "vals": vals}
    return {"inds": inds, "vals": vals}


__sparsewrapnormalpdf.computed = {}


def __largestMOS(gen, cutoff, equiv):
    best = 1
    gen = gen + np.random.uniform(0.0, 0.001)   # randomness good for EDOs
    for n in range(1, cutoff+1):
        scl = np.r_[np.sort(gen*np.r_[0:n] % equiv), equiv]
        d = len(np.unique(np.round(np.diff(scl), 6)))
        if d == 2:
            best = n
    return best


########################################################################
# MAIN FUNCTIONS
########################################################################
def ChordsFromScale(scale, probs, n, transpose=False, condense=False,
                    eps=1e-6, equiv=1200):
    """
    Returns a list of all the probability-weighted chords from a given scale.

    The input scale is given by `refscale`, with probabilities given by
    `probs`. The parameter `n` specifies the cardinality of each chord.

    If `probs` is set to None, no probabilities are returned.

    If the parameter `transpose` is False (default), this function simply
    generates the set of all n-tuples of notes in the scale. This is useful
    when evaluating scales which are not typically transposed in musical
    practice (such as Slendro or Pelog).

    If `transpose` is True, this instead generates the set of all n-size chords
    appearing *anywhere* in the scale. In this situation, rather than giving an
    n-tuple of notes, we instead give an (n-1)-tuple of interval sizes from
    each note to the first note in the chord.

    For example, if your input is the major mode of the diatonic scale and
    `n`=3, if `transpose` is False, you will get the chord (400, 700, 1100). If
    `transpose` is True, you will instead get the chord (300, 700), which is
    the same chord, just representing the relative sizes of each of the last
    two notes relative to the first one. The latter represents a
    transposition-invariant minor chord.

    Note that if `transpose` is True, you should expect to see the same chord
    more than once, if it appears more than once in the scale. For example, in
    the diatonic major mode, the minor chord appears at (400, 700, 1100) but
    also at (200, 500, 900) and (900, 0, 400) (assuming an equivalence interval
    of 1200 cents). If `transpose` is True, all three of these will both map to
    the minor triad at (300, 700), which will appear three times in the result.
    This is the intended result and is important when interpreting the CE of a
    chord, which not only tells you how "blurry" the chord is, but how good it
    is at telling you where you're located within a scale.

    The parameter `condense` reduces duplicate entries by summing their
    probabilities. So in the above example, all occurrences of (300, 700) will
    be reduced to a single entry which is the sum of all probabilities. This is
    only recommended when computing CMI or CCC, as it can make it faster.

    The parameter `tol` sets the tolerance for floating-point arithmetic. This
    is 1e-10 by default.

    The parameter `equiv` sets the interval of equivalence (used when reducing
    chords). It is set to 1200 cents by default.

    This function assumes that the probability of playing each chord is equal
    to the probability of playing all of the notes as independent events (so
    the product of the individual probabilities of the notes.) If you want to
    replace this with a different distribution, you can simply use the list of
    chords returned by this function and use your own probability distribution.

    This function returns a tuple (scale, probabilities).
    """
    scale, probs = __numpify(scale, probs)

    # iterate through the n'th cartesian power of the scale, get the chord
    # notes and product of probabilities, store in outchords and outprobs.
    # faster if outs are first python list, then converted to ndarray at end

    outchords = []
    outprobs = []

    # TODO - maybe parallelize chord generation (need shared read)
    for comb in itertools.product(range(0, len(scale)), repeat=n):
        # note: it's scale[x][0] because after numpifying, scale looks like
        # np.array([[0], [200], [400], [500], [700], [900], [1100]])
        chord = [round(scale[x][0]/eps)*eps for x in comb]

        if probs is not None:
            prob = np.prod([probs[x] for x in comb])

        if transpose:
            chord = [__wrap(x - chord[0], equiv) for x in chord[1:]]

        if condense and chord in outchords:
            if probs is not None:
                outprobs[outchords.index(chord)] += prob
        else:
            outchords += [chord]
            if probs is not None:
                outprobs += [prob]

    if probs is not None:
        outprobs = __numpify(outprobs)
    else:
        outprobs = None

    outchords = __numpify(outchords)
    return outchords, outprobs


def CE(chords, reflist, probs, s, a=1, transpose=False, equiv=1200):
    """
    Computes the Categorical Entropy of a set of n-ads (chords), with respect
    to a reference list of n-adic chords and probability distribution on them.
    The Categorical Entropy tells you how easily the chords can be matched
    to the reference list.

    Typically, the "chords" will be a set of notes (monads), dyads or triads,
    being computed with respect to all the notes/dyads/triads taken from some
    scale. Notably, if the cardinality of the chords is 1 (as in the case of
    monads or transposition-invariant dyads), `chords` can simply be a list of
    cents from 0 to 1200 to get the whole CE spectrum for the scale.

    The chords we are computing the entropy of are given by the variable
    `chords`. In the information-theoretic description of CE, these are taken
    from the random variable Y. This can be either a list of lists or a 2D
    numpy array, where the first axis is the entry in the list and the second
    is the entry in the chord. Each chord must be of the same cardinality.

    The reference list of chords and the probabilities are given by `reflist`
    and `probs`. In the information-theoretic model of CE, these together
    describe the random variable X. The chords can be either list of lists or a
    2D numpy array, and the probabilities can be either a list or a 1D numpy
    array. The reflist must be the same cardinality as the chords.

    For now, a wrapped Gaussian distribution is used. `s` gives the standard
    deviation.

    This function can compute the Renyi entropy of order `a`, which is set to 1
    (the Shannon entropy) by default.

    The `transpose` parameter specifies whether these chords are transposition-
    invariant or not. It is set to False by default. If set to True, it will
    change the covariance matrix accordingly. Note that the effective value of
    `s` increases if `transpose`=True; it is multiplied by the square root of
    the cardinality of the chord.

    The `equiv` parameter gives an interval of equivalence -- set to 1200 cents
    by default.

    The result is a numpy array of the categorical entropies H(X|Y=y) of the
    chords.
    """

    # sanity check chords and scale sizes
    chords, reflist, probs = __numpify(chords, reflist, probs)
    assert np.shape(chords)[1] == np.shape(reflist)[1], \
        "CE error - chords and reflist must be the same size!"
    assert np.shape(reflist)[0] == np.shape(probs)[0], \
        "CE error - reflist and probs must be the same size!"

    # reduce by equivalence interval
    chords = __wrap(chords, equiv)
    reflist = __wrap(reflist, equiv)

    def one_chord(chord):
        # get the vector difference between the chord and each reference, then
        # get its value on the Gaussian (norm of Gaussians of individual
        # coordinates), multiply by the probability of each chord,
        # normalize so the probabilities sum to 1, then get the entropy of the
        # result

        diffs = __wrap(np.abs(chord - reflist), equiv)
        P_XcY = __wrapnormalpdf(diffs, s, transpose, equiv) * probs[:, 0]
        P_XcY = P_XcY/P_XcY.sum()
        return __entropy(P_XcY, a)

    # H_YcX is a list of H(Y|X)'s, the categorical entropy
    H_YcX = [one_chord(chord) for chord in chords] # -- non-parallel
    # H_YcX = Parallel(n_jobs=-1)(delayed(one_chord)(c) for c in chords)

    return np.array(H_YcX)


def CEFromScale(chords, scale, probs, s, n, a=1, transpose=False, equiv=1200):
    """
    Computes the Categorical Entropy of a set of n-ads (chords), with respect
    to a scale the n-adic chords will be drawn from, and a probability
    distribution on that scale.

    This is a convenience function that auto-generates a list of size-n chords
    and probabilities from your scale and distribution, and calls CE() with it.

    See the documentation for CE() for more info. This function is exactly the
    same, except instead of submitting a `reflist` of chords and probabilities,
    you instead submit a single `scale` and probabilities, and it
    auto-generates the reflist for you.

    The `tol` parameter sets the tolerance for floating-point arithmetic.
    """

    derivedreflist, derivedprobs = ChordsFromScale(scale, probs, n, transpose,
                                                   condense=False, equiv=equiv)
    return CE(chords, derivedreflist, derivedprobs, s, a, transpose, equiv)


def CMI(reflist, probs, s, n, a=1, transpose=False, equiv=1200, incr=1,
        tol=1e-10, sparse=False):
    """
    Computes the n-adic Categorical Mutual Information of a set of n-ads
    (chords) and a probability distribution on them. The Categorical Mutual
    Information can be thought of as telling you how easily it is to
    distinguish the n-ads from one another even in the presence of mistuning.

    Typically, the "chords" will be a set of notes (monads), dyads or triads,
    taken from some scale. However, this function is more general in that it
    computes the CMI of any arbitrary set of chords, which can be useful.

    The reference list of chords and the probabilities are given by `reflist`
    and `probs`. In the information-theoretic model of CE, these together
    describe the random variable X. The chords can be either list of lists or a
    2D numpy array, and the probabilities can be either a list or a 1D numpy
    array. The reflist must be the same cardinality as the chords.

    For now, a wrapped Gaussian distribution is used. `s` gives the standard
    deviation.

    This function can compute the Renyi CMI of order `a`, which is set to 1
    (the Shannon entropy) by default.

    The `transpose` parameter specifies whether these chords are transposition-
    invariant or not. It is set to False by default. If set to True, it will
    change the covariance matrix accordingly. Note that the effective value of
    `s` increases if `transpose`=True; it is multiplied by the square root of
    the cardinality of the chord.

    The `equiv` parameter gives an interval of equivalence -- set to 1200 cents
    by default.

    The `incr` parameter tells us how fine we would like our grid in cents to
    be. If `incr`=1, we look at cents in increments of 1 to the equivalence
    interval. If `incr`=0.1, we instead look at cents in increments of 0.1.

    The `tol` parameter tells us how low the Gaussian amplitude gets, relative
    to its value at 0, before we quantize to zero. This is set to 1e-10 by
    default.

    The `sparse` parameter determines whether we use dense or sparse vectors.
    Sparse vectors can be useful for higher values of `n`.

    The result is a scalar representing the Categorical Mutual Information
    I(X;Y) of the chords.
    """

    # sanity check chords and scale sizes
    reflist, probs = __numpify(reflist, probs)
    assert np.shape(reflist)[0] == np.shape(probs)[0], \
        "CE error - reflist and probs must be the same size!"
    assert abs(equiv/incr - round(equiv/incr)) < tol, \
        "ERROR: incr must divide equiv"
    probs = probs/probs.sum()

    chordsize = np.shape(reflist)[1]

    # now create sparse Gaussian
    g_pdf = __sparsewrapnormalpdf(s, chordsize, incr, tol=tol,
                                  transpose=transpose, equiv=equiv)

    # add Gaussians
    g_sum = None
    axislen = round(equiv/incr)
    dim = n if not transpose else n-1

    # TODO - maybe parallelize Gaussian sums (but shared write necessary)
    for chord, prob in zip(reflist, probs):
        tensor_inds = np.round(__wrap(g_pdf['inds']+chord, equiv)/incr)\
                          .astype(int) % axislen
        tensor_vals = g_pdf['vals'] * prob[0]

        if sparse:
            g_tmp = tf.SparseTensor(indices=tensor_inds,
                                    values=tensor_vals,
                                    dense_shape=[axislen]*dim)
        else:
            tensor_inds = tuple(tensor_inds.T)
            g_tmp = np.zeros([axislen]*dim)
            g_tmp[tensor_inds] = tensor_vals

        if g_sum is None:
            g_sum = g_tmp
        else:
            g_sum = tf.sparse.add(g_sum, g_tmp) if sparse else g_sum + g_tmp

    H_Y = __entropy(g_sum, a)
    H_YcX = __entropy(g_pdf['vals'], a)

    return H_Y - H_YcX


def CMIFromScale(scale, probs, s, n, a=1, transpose=False, equiv=1200, incr=1,
                 tol=1e-10, sparse=False):
    """
    Computes the Categorical Mutual Information of a set of n-ads (chords),
    with respect to a scale the n-adic chords will be drawn from, and a
    probability distribution on that scale.

    This is a convenience function that auto-generates a list of size-n chords
    and probabilities from your scale and distribution, and calls CMI() with
    it.

    See the documentation for CMI() for more info. This function is exactly the
    same, except instead of submitting a `reflist` of chords and probabilities,
    you instead submit a single `scale` and probabilities, and it
    auto-generates the reflist for you.
    """

    scale = np.round(scale/incr)*incr

    derivedreflist, derivedprobs = ChordsFromScale(scale, probs, n, transpose,
                                                   condense=True, equiv=equiv)
    out = CMI(derivedreflist, derivedprobs, s, n, a, transpose, equiv, incr,
              tol, sparse)
    return out


def CMIFromMOSGenerators(gens, notes, per, probs, s, n, transpose, incr=1, a=1,
                         tol=1e-10, sparse=False, at_most=False, equiv=1200):
    """
    Computes the n-adic Categorical Mutual Information of a set of MOS's, given
    the MOS generators and starting probabilities.

    The parameter `gens` is a 1D numpy array. For example, np.r_[686:720:35j]
    gives all diatonic scale generators.

    The parameter `notes` determines how many notes are in the MOS for each
    generator. This should be a scalar, or a complex number specifying the
    largest allowable MOS for each generator. For example, if notes = 10j, this
    says "at most decatonic" MOS's for each generator. If the largest MOS
    generated by some generator is less than 10, it will use the largest.

    The parameter `per` is the number of periods per octave.

    The parameter `probs` is a probability distribution on the MOS. However,
    if the number of notes is variable (such as if you use a complex number
    for `notes` above), this should be set to None, and the uniform
    distribution will be used by default.

    The other parameters (s, n, transpose, incr, a, res, tol, sparse) are the
    same as in CMI, so see the documentation for that for more info.

    This returns a numpy array of CMI's I(X;Y) for each scale.
    """

    def one_generator(gen):
        if at_most:
            newnotes = __largestMOS(gen, notes, equiv)
        else:
            newnotes = per*int(notes/per)

        scl = np.array([])
        for x in range(0, per):
            scl = np.r_[(equiv/per)+scl,
                        (gen * np.r_[0:int(newnotes/per)]) % (equiv/per)]
        if probs is None:
            p = np.ones(newnotes)/newnotes
        else:
            p = probs
        print("Computing %f[%d] (s=%g, a=%g, transpose=%s)..."
              % (gen, newnotes, s, a, transpose))
        return CMIFromScale(scl, p, s, n, a, transpose, equiv=equiv,
                            tol=tol, incr=incr, sparse=sparse)

    # results = [one_generator(g) for g in gens] # - non-parallel
    results = Parallel(n_jobs=-1)(delayed(one_generator)(g) for g in gens)
    return np.array(results)


def CMIFromEDOs(EDOs, probs, s, n, transpose, incr=1, a=1, tol=1e-10,
                sparse=False, equiv=1200):
    """
    Computes the n-adic Categorical Mutual Information of a set of MOS's, given
    the MOS generators and starting probabilities.

    The parameter `gens` is a 1D numpy array. For example, np.r_[686:720:35j]
    gives all diatonic scale generators.

    The parameter `notes` determines how many notes are in the MOS for each
    generator. This should be a scalar, or a complex number specifying the
    largest allowable MOS for each generator. For example, if notes = 10j, this
    says "at most decatonic" MOS's for each generator. If the largest MOS
    generated by some generator is less than 10, it will use the largest.

    The parameter `probs` is a probability distribution on the MOS. However,
    if the number of notes is variable (such as if you use a complex number
    for `notes` above), this should be set to None, and the uniform
    distribution will be used by default.

    The other parameters (s, n, transpose, incr, a, res, tol, sparse) are the
    same as in CMI, so see the documentation for that for more info.

    This returns a numpy array of CMI's I(X;Y) for each scale.
    """

    def one_generator(EDO):
        gen = 1200/EDO
        scl = (gen * np.r_[0:EDO]) % equiv
        if probs is None:
            p = np.ones(EDO)/EDO
        else:
            p = probs
        print("Computing %f[%d] (s=%g, a=%g, transpose=%s)..."
              % (gen, EDO, s, a, transpose))
        return CMIFromScale(scl, p, s, n, a, transpose, equiv=equiv,
                            tol=tol, incr=incr, sparse=sparse)

    # results = [one_generator(e) for e in EDOs] - non-parallel
    results = Parallel(n_jobs=-1)(delayed(one_generator)(e) for e in EDOs)
    return np.array(results)


def CCC(reflist, probs, s, n, a=1, transpose=False, equiv=1200, incr=1,
        tol=1e-10, sparse=False, method="SLSQP", maxiter=100):
    """
    Computes the n-adic Categorical Channel Capacity of a set of n-ads
    (chords) and a probability distribution on them. The Categorical Channel
    Capacity can be thought of as telling you how easily it is to
    distinguish the n-ads from one another even in the presence of mistuning,
    assuming the *best* probability distribution on your n-ads.

    For more information, look at the documentation for CMI. The arguments
    to this function are exactly the same, with one exception: the parameter
    `probs` now just gives a beginning guess for the probability distribution,
    as a starting point for CCC to minimize.

    If `probs` is set to "None", then the uniform distribution is used.

    The result is a scalar representing the Categorical Channel Capacity
    C(X;Y) of the chords.
    """

    assert a > 1, "ERROR: CCC needs a > 1. (If you want a = 1, try a = 1.001)"

    # if a is 2, use the pseudoinverse!
    if a == 2:
        pass
    # optimize the norm
    def optfunc(p):
        ##@! NOTE - after fidgeting, the below seems to optimize fastest
        # return 2**((1-a)/a
        #            * CMI(reflist, np.abs(p)/np.abs(p).sum(), s, n, a,
        #                  transpose, equiv, incr, tol, sparse))
        return -2**(CMI(reflist, np.abs(p)/np.abs(p).sum(), s, n, a,
                       transpose, equiv, incr, tol, sparse))
        # return -CMI(reflist, np.abs(p)/np.abs(p).sum(), s, n, a,
        #             transpose, equiv, incr, tol, sparse)

    # if probs isn't set, startprob is uniform distribution
    if probs is None:
        startprob = np.ones(np.shape(reflist)[0])/np.shape(reflist)[0]
    else:
        startprob = probs

    # probably pointless, since this seems broken
    if method == "trust-constr":
        hess = optimize.SR1
    elif method == "SLSQP":
        hess = None
    else:
        hess = "2-point"

    # also probably pointless, since SLSQP is fastest
    if method == "COBYLA":
        cons = ({'type': 'ineq',
                 'fun': lambda x: np.abs(x).sum()-0.9},
                {'type': 'ineq',
                 'fun': lambda x: 1.1-np.abs(x).sum()})
    elif method == "SLSQP":
        cons = ({'type': 'eq',
                 'fun': lambda x: np.abs(x).sum() - 1})
    else:
        cons = {}

    # do the minimization
    bestprobs = optimize.minimize(optfunc, startprob,
                                  method=method,
                                  jac="2-point",
                                  hess=hess,
                                  tol=tol,
                                  options={'disp': True, 'maxiter': maxiter},
                                  constraints=cons)

    # return entropy
    # res = a/(1-a) * np.log2(optfunc(bestprobs.x)) ##@! old version
    res = np.log2(-optfunc(bestprobs.x))
    # res = -optfunc(bestprobs.x)
    return bestprobs, res


def CCCFromScale(scale, s, n, a=1, transpose=False, equiv=1200, incr=1,
                 tol=1e-10, sparse=False, method="SLSQP", maxiter=100):
    """
    Computes the Categorical Channel Capacity of a set of n-ads (chords),
    with respect to a scale the n-adic chords will be drawn from.

    This is a convenience function that auto-generates a list of size-n chords
    and calls CCC() with it.

    See the documentation for CCC() for more info. This function is exactly the
    same, except instead of submitting a `reflist` of chords you instead submit
    a single `scale`, and it auto-generates the reflist for you. No
    probabilities are given, since CCC() will find the best probabilities.

    The only new parameter is `method`, which determines the method to pass to
    scipy.optimize.minimize() (set to "SLSQP" by default).
    """

    scale = np.round_(a=scale/incr)*incr

    derivedreflist, _ = ChordsFromScale(scale, None, n, transpose,
                                        condense=True, equiv=equiv)
    bestprobs, res = CCC(derivedreflist, None, s, n, a, transpose, equiv, incr,
                         tol, sparse, method, maxiter)
    return bestprobs, res


def CCCFromMOSGenerators(gens, notes, per, s, n, transpose, incr=1, a=1,
                         tol=1e-10, sparse=False, at_most=False, maxiter=100,
                         equiv=1200):
    """
    Computes the n-adic Categorical Channel Capacity of a set of MOS's, given
    the MOS generators.

    The parameter `gens` is a 1D numpy array. For example, np.r_[686:720:35j]
    gives all diatonic scale generators.

    The parameter `notes` determines how many notes are in the MOS for each
    generator. This should be an int.

    If the parameter `at_most` = True, this gives you the largest MOS that is
    no greater than the `notes` parameter. For example, if notes = 10, and
    at_most = True, this says "at most decatonic" MOS's for each generator.
    If the largest MOS generated by some generator is less than 10, it will use
    the largest.

    No probabilities are given, as CCC finds the best probabilities.

    The other parameters (s, n, transpose, incr, a, res, tol, sparse) are the
    same as in CCC and CMI. See the documentation for CMI() for more info.

    This returns a numpy array of CCC's C(X;Y) for each scale.
    """

    def one_generator(gen):
        if at_most:
            newnotes = __largestMOS(gen, notes, equiv)
        else:
            newnotes = per*int(notes/per)

        scl = np.array([])
        for x in range(0, per):
            scl = np.r_[(equiv/per)+scl,
                        (gen * np.r_[0:int(newnotes/per)]) % (equiv/per)]
        print("Computing %f[%d] (s=%g, a=%g, transpose=%s)..."
              % (gen, newnotes, s, a, transpose))
        return CCCFromScale(scl, s, n, a, transpose, equiv=equiv,
                            tol=tol, incr=incr, sparse=sparse, maxiter=maxiter)

    # results = [one_generator(g) for g in gens] # - non-parallel ##@!
    results = Parallel(n_jobs=-1)(delayed(one_generator)(g) for g in gens)
    bestprobs = [x[0] for x in results]
    CCCs = np.array([x[1] for x in results])
    return bestprobs, CCCs
