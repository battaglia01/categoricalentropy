#!/usr/bin/env python3
"""
This file contains tests and demonstrations of various types of categorical
channel capacity. In particular, this file tests the categorical channel
capacity of MOS's.

The global value S_ALL sets the value of s for all demos (makes it easy to
change them all at once).
"""

from CEplot import do_CCC_gens
import numpy as np
import sys

# GLOBALS
S_ALL = [10, 12.5, 15, 17.5, 20]    # global so we can change all easily

# Tests to do




diatonic_spectrum = False
chromatic_spectrum = False
mavila7_spectrum = True
mavila9_spectrum = True
mavila16_spectrum = True
porcupine7_spectrum = False
porcupine8_spectrum = False
porcupine15_spectrum = False
neutral7_spectrum = False
neutral10_spectrum = False
neutral17_spectrum = False
blackwood10_spectrum = False
blackwood15_spectrum = False
at_most_decatonic_spectrum = False


def writenow(str):
    sys.stderr.flush()
    sys.stdout.flush()
    sys.stdout.write(str + "\n")
    sys.stderr.flush()
    sys.stdout.flush()


######################################################
# DIATONIC MOS SPECTRUM
######################################################
if diatonic_spectrum:
    gens = np.r_[686:720:341j]  # 7 to 12-EDO. 341j means 0.1 cent incr
    do_CCC_gens(gens, notes=7, s=S_ALL, n=2, a=1.001, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                max_iter=1000, title="Diatonic MOS spectrum")

######################################################
# CHROMATIC MOS SPECTRUM
######################################################
if chromatic_spectrum:
    gens = np.r_[686:720:341j]  # 7 to 12-EDO. 341j means 0.1 cent incr
    do_CCC_gens(gens, notes=12, s=S_ALL, n=2, a=1.001, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                max_iter=1000, title="Chromatic MOS spectrum")

######################################################
# MAVILA[7] MOS SPECTRUM
######################################################
if mavila7_spectrum:
    gens = np.r_[666.7:685.7:191j]  # 9 to 7-EDO. 191j means 0.1 cent incr
    do_CCC_gens(gens, notes=7, s=S_ALL, n=2, a=1.001, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                max_iter=1000, title="Mavila[7] MOS spectrum")

######################################################
# MAVILA[9] MOS SPECTRUM
######################################################
if mavila9_spectrum:
    gens = np.r_[666.7:685.7:191j]  # 9 to 7-EDO. 191j means 0.1 cent incr
    do_CCC_gens(gens, notes=9, s=S_ALL, n=2, a=1.001, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                max_iter=1000, title="Mavila[9] MOS spectrum")

######################################################
# MAVILA[16] MOS SPECTRUM
######################################################
if mavila16_spectrum:
    gens = np.r_[666.7:685.7:191j]  # 9 to 7-EDO. 191j means 0.1 cent incr
    do_CCC_gens(gens, notes=16, s=S_ALL, n=2, a=1.001, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                max_iter=1000, title="Mavila[16] MOS spectrum")

######################################################
# PORCUPINE[7] MOS SPECTRUM
######################################################
if porcupine7_spectrum:
    gens = np.r_[150:171.4:215j]  # 8 to 7-EDO. 215j means 0.1 cent incr
    do_CCC_gens(gens, notes=7, s=S_ALL, n=2, a=1.001, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                max_iter=1000, title="Porcupine[7] MOS spectrum")

######################################################
# PORCUPINE[8] MOS SPECTRUM
######################################################
if porcupine8_spectrum:
    gens = np.r_[150:171.4:215j]  # 8 to 7-EDO. 215j means 0.1 cent incr
    do_CCC_gens(gens, notes=8, s=S_ALL, n=2, a=1.001, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                max_iter=1000, title="Porcupine[8] MOS spectrum")

######################################################
# PORCUPINE[15] MOS SPECTRUM
######################################################
if porcupine15_spectrum:
    gens = np.r_[150:171.4:215j]  # 8 to 7-EDO. 215j means 0.1 cent incr
    do_CCC_gens(gens, notes=15, s=S_ALL, n=2, a=1.001, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                max_iter=1000, title="Porcupine[15] MOS spectrum")

######################################################
# NEUTRAL[7] MOS SPECTRUM
######################################################
if neutral7_spectrum:
    gens = np.r_[342.9:360:172j]  # 7 to 10-EDO. 172j means 0.1 cent incr
    do_CCC_gens(gens, notes=7, s=S_ALL, n=2, a=1.001, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                max_iter=1000, title="Neutral[7] MOS spectrum")

######################################################
# NEUTRAL[10] MOS SPECTRUM
######################################################
if neutral10_spectrum:
    gens = np.r_[342.9:360:172j]  # 7 to 10-EDO. 172j means 0.1 cent incr
    do_CCC_gens(gens, notes=10, s=S_ALL, n=2, a=1.001, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                max_iter=1000, title="Neutral[10] MOS spectrum")

######################################################
# NEUTRAL[17] MOS SPECTRUM
######################################################
if neutral17_spectrum:
    gens = np.r_[342.9:360:172j]  # 7 to 10-EDO. 172j means 0.1 cent incr
    do_CCC_gens(gens, notes=17, s=S_ALL, n=2, a=1.001, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                max_iter=1000, title="Neutral[17] MOS spectrum")

######################################################
# BLACKWOOD[10] MOS SPECTRUM
######################################################
if blackwood10_spectrum:
    gens = np.r_[0:120:1201j]  # 5 to 10-EDO. 1201j means 1 cent incr
    do_CCC_gens(gens, notes=10, per=5, s=S_ALL, n=2, a=1.001, transpose=True,
                incr=0.1, tol=1e-10, sparse=False, at_most=False, xtickres=10,
                max_iter=1000, title="Blackwood[10] MOS spectrum")

######################################################
# BLACKWOOD[15] MOS SPECTRUM
######################################################
if blackwood15_spectrum:
    gens = np.r_[0:120:1201j]  # 5 to 10-EDO. 1201j means 0.1 cent incr
    do_CCC_gens(gens, notes=15, per=5, s=S_ALL, n=2, a=1.001, transpose=True,
                incr=0.1, tol=1e-10, sparse=False, at_most=False, xtickres=10,
                max_iter=1000, title="Blackwood[15] MOS spectrum")

######################################################
# AT-MOST-DECATONIC MOS SPECTRUM
######################################################
if at_most_decatonic_spectrum:
    gens = np.r_[0:600:601j]  # 0 to 600
    do_CCC_gens(gens, notes=10, s=S_ALL, n=2, a=1.001, transpose=True, incr=1,
                tol=1e-10, sparse=False, at_most=True, xtickres=100,
                max_iter=1000, title="At-most-decatonic MOS spectrum")
