#!/usr/bin/env python3
"""
This file contains tests and demonstrations of various types of categorical
mutual information. In particular, this file tests the categorical mutual
information of MOS's.

The global value S_ALL sets the value of s for all demos (makes it easy to
change them all at once).
"""

from CEplot import do_CMI_gens, do_CMI_EDOs
import numpy as np

# GLOBALS
S_ALL = [10, 12.5, 15, 17.5, 20]    # global so we can change all easily

# Tests to do
monadic_EDO_spectrum = False
monadic_diatonic_spectrum = False
monadic_chromatic_spectrum = False
diatonic_spectrum = False
chromatic_spectrum = True
mavila7_spectrum = False
mavila9_spectrum = False
mavila16_spectrum = False
porcupine7_spectrum = False
porcupine8_spectrum = False
porcupine15_spectrum = False
neutral7_spectrum = False
neutral10_spectrum = False
neutral17_spectrum = False
blackwood10_spectrum = False
blackwood15_spectrum = False
pajara10_spectrum = False
pajara12_spectrum = False
at_most_heptatonic_spectrum = False
at_most_decatonic_spectrum = False
at_most_24_spectrum = False

######################################################
# MONADIC EDO SPECTRUM
######################################################
if monadic_EDO_spectrum:
    EDOs = np.r_[1:50]  # 0 to 600
    do_CMI_EDOs(EDOs, s=S_ALL, n=1, a=1, transpose=False, incr=1,
                tol=1e-10, sparse=False, xtickres=1,
                title="EDO spectrum")

######################################################
# MONADIC DIATONIC SPECTRUM
######################################################
if monadic_diatonic_spectrum:
    gens = np.r_[686:720:341j]  # 7 to 12-EDO. 341j means 0.1 cent incr
    do_CMI_gens(gens, notes=7, s=S_ALL, n=1, a=1, transpose=False, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Diatonic MOS spectrum")

######################################################
# MONADIC CHROMATIC SPECTRUM
######################################################
if monadic_chromatic_spectrum:
    gens = np.r_[686:720:341j]  # 7 to 12-EDO. 341j means 0.1 cent incr
    do_CMI_gens(gens, notes=12, s=S_ALL, n=1, a=1, transpose=False, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Chromatic MOS spectrum")

######################################################
# DIATONIC MOS SPECTRUM (ALL TRANSPOSED FROM HERE BELOW)
######################################################
if diatonic_spectrum:
    gens = np.r_[686:720:341j]  # 7 to 12-EDO. 341j means 0.1 cent incr
    do_CMI_gens(gens, notes=7, s=S_ALL, n=2, a=1, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Diatonic MOS spectrum")

######################################################
# CHROMATIC MOS SPECTRUM
######################################################
if chromatic_spectrum:
    gens = np.r_[686:720:341j]  # 7 to 12-EDO. 341j means 0.1 cent incr
    do_CMI_gens(gens, notes=12, s=S_ALL, n=2, a=1, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Chromatic MOS spectrum")

######################################################
# MAVILA[7] MOS SPECTRUM
######################################################
if mavila7_spectrum:
    gens = np.r_[666.7:685.7:191j]  # 9 to 7-EDO. 191j means 0.1 cent incr
    do_CMI_gens(gens, notes=7, s=S_ALL, n=2, a=1, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Mavila[7] MOS spectrum")

######################################################
# MAVILA[9] MOS SPECTRUM
######################################################
if mavila9_spectrum:
    gens = np.r_[666.7:685.7:191j]  # 9 to 7-EDO. 191j means 0.1 cent incr
    do_CMI_gens(gens, notes=9, s=S_ALL, n=2, a=1, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Mavila[9] MOS spectrum")

######################################################
# MAVILA[16] MOS SPECTRUM
######################################################
if mavila16_spectrum:
    gens = np.r_[666.7:685.7:191j]  # 9 to 7-EDO. 191j means 0.1 cent incr
    do_CMI_gens(gens, notes=16, s=S_ALL, n=2, a=1, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Mavila[16] MOS spectrum")

######################################################
# PORCUPINE[7] MOS SPECTRUM
######################################################
if porcupine7_spectrum:
    gens = np.r_[150:171.4:215j]  # 8 to 7-EDO. 215j means 0.1 cent incr
    do_CMI_gens(gens, notes=7, s=S_ALL, n=2, a=1, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Porcupine[7] MOS spectrum")

######################################################
# PORCUPINE[8] MOS SPECTRUM
######################################################
if porcupine8_spectrum:
    gens = np.r_[150:171.4:215j]  # 8 to 7-EDO. 215j means 0.1 cent incr
    do_CMI_gens(gens, notes=8, s=S_ALL, n=2, a=1, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Porcupine[8] MOS spectrum")

######################################################
# PORCUPINE[15] MOS SPECTRUM
######################################################
if porcupine15_spectrum:
    gens = np.r_[150:171.4:215j]  # 8 to 7-EDO. 215j means 0.1 cent incr
    do_CMI_gens(gens, notes=15, s=S_ALL, n=2, a=1, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Porcupine[15] MOS spectrum")

######################################################
# NEUTRAL[7] MOS SPECTRUM
######################################################
if neutral7_spectrum:
    gens = np.r_[342.9:360:172j]  # 7 to 10-EDO. 172j means 0.1 cent incr
    do_CMI_gens(gens, notes=7, s=S_ALL, n=2, a=1, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Neutral[7] MOS spectrum")

######################################################
# NEUTRAL[10] MOS SPECTRUM
######################################################
if neutral10_spectrum:
    gens = np.r_[342.9:360:172j]  # 7 to 10-EDO. 172j means 0.1 cent incr
    do_CMI_gens(gens, notes=10, s=S_ALL, n=2, a=1, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Neutral[10] MOS spectrum")

######################################################
# NEUTRAL[17] MOS SPECTRUM
######################################################
if neutral17_spectrum:
    gens = np.r_[342.9:360:172j]  # 7 to 10-EDO. 172j means 0.1 cent incr
    do_CMI_gens(gens, notes=17, s=S_ALL, n=2, a=1, transpose=True, incr=0.1,
                tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Neutral[17] MOS spectrum")

######################################################
# BLACKWOOD[10] MOS SPECTRUM
######################################################
if blackwood10_spectrum:
    gens = np.r_[0:120:1201j]  # 5 to 10-EDO. 121j means 1 cent incr
    do_CMI_gens(gens, notes=10, per=5, s=S_ALL, n=2, a=1, transpose=True,
                incr=0.1, tol=1e-10, sparse=False, at_most=False, xtickres=10,
                title="Blackwood[10] MOS spectrum")

######################################################
# BLACKWOOD[15] MOS SPECTRUM
######################################################
if blackwood15_spectrum:
    gens = np.r_[0:120:1201j]  # 5 to 10-EDO. 172j means 0.1 cent incr
    do_CMI_gens(gens, notes=15, per=5, s=S_ALL, n=2, a=1, transpose=True,
                incr=0.1, tol=1e-10, sparse=False, at_most=False, xtickres=10,
                title="Blackwood[15] MOS spectrum")

######################################################
# PAJARA[10] MOS SPECTRUM
######################################################
if pajara10_spectrum:
    gens = np.r_[100:120:201j]  # 10 to 12-EDO. 201j means 0.1 cent incr
    do_CMI_gens(gens, notes=10, per=2, s=S_ALL, n=2, a=1, transpose=True,
                incr=0.1, tol=1e-10, sparse=False, at_most=False, xtickres=10,
                title="Pajara[10] MOS spectrum")

######################################################
# PAJARA[12] MOS SPECTRUM
######################################################
if pajara12_spectrum:
    gens = np.r_[100:120:201j]  # 10 to 12-EDO. 201j means 0.1 cent incr
    do_CMI_gens(gens, notes=12, per=2, s=S_ALL, n=2, a=1, transpose=True,
                incr=0.1, tol=1e-10, sparse=False, at_most=False, xtickres=10,
                title="Pajara[12] MOS spectrum")

######################################################
# AT-MOST-HEPTATONIC MOS SPECTRUM
######################################################
if at_most_heptatonic_spectrum:
    gens = np.r_[0:600:601j]  # 0 to 600
    do_CMI_gens(gens, notes=7, s=S_ALL, n=2, a=1, transpose=True, incr=1,
                tol=1e-10, sparse=False, at_most=True, xtickres=100,
                title="At-most-heptatonic MOS spectrum")

######################################################
# AT-MOST-DECATONIC MOS SPECTRUM
######################################################
if at_most_decatonic_spectrum:
    gens = np.r_[0:600:601j]  # 0 to 600
    do_CMI_gens(gens, notes=10, s=S_ALL, n=2, a=1, transpose=True, incr=1,
                tol=1e-10, sparse=False, at_most=True, xtickres=100,
                title="At-most-decatonic MOS spectrum")

######################################################
# AT-MOST-HEPTATONIC MOS SPECTRUM
######################################################
if at_most_24_spectrum:
    gens = np.r_[0:600:601j]  # 0 to 600
    do_CMI_gens(gens, notes=24, s=S_ALL, n=2, a=1, transpose=True, incr=1,
                tol=1e-10, sparse=False, at_most=True, xtickres=100,
                title="At-most-24-note MOS spectrum")
