#!/usr/bin/env python3
"""
This file contains tests and demonstrations of various types of categorical
entropy.

In particular, this file tests "2D" categorical entropy:
- Dyadic CE
- Triadic CE with transpositional-invariance

The global value S_ALL sets the value of s for all demos (makes it easy to
change them all at once).
"""

from CEplot import do_CE
import numpy as np

# GLOBALS
S_ALL = 20    # value of s, global so we can change all easily
INCR_ALL = 5  # cents increment

# Tests to do
dyadic_tests = False
diatonic_tests = False
porcupine7_tests = False
porcupine8_tests = False
mavila7_tests = True


######################################################
# DYADIC NON-TRANSPOSED
######################################################
if dyadic_tests:
    # ----------------------------------------------------
    # get dyadic CE of 12-edo, non-transposed
    scale = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100]
    probs = np.ones(12)/12
    do_CE(scale, probs, s=S_ALL, n=2, transpose=False, incr=INCR_ALL,
          title='12-EDO')

    # ----------------------------------------------------
    # get CE of 12-edo rotated diatonic scale
    fifth = 700
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=False, incr=INCR_ALL,
          title="12-EDO diatonic scale")

    # ----------------------------------------------------
    # get CE of 31-edo rotated diatonic scale
    fifth = 697
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=False, incr=INCR_ALL,
          title="31-EDO diatonic scale")


######################################################
# DIATONIC TRANSPOSED
######################################################
if diatonic_tests:
    # ----------------------------------------------------
    # get triadic fCE of 12-edo rotated diatonic scale
    fifth = 700
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="12-EDO diatonic scale")

    # ----------------------------------------------------
    # get triadic CE of 31-edo rotated diatonic scale
    fifth = 697
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="31-EDO diatonic scale")

    # ----------------------------------------------------
    # get triadic CE of 19-edo rotated diatonic scale
    fifth = 695
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="19-EDO diatonic scale")

    # ----------------------------------------------------
    # get triadic CE of 17-edo rotated diatonic scale
    fifth = 706
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="17-EDO diatonic scale")

    # ----------------------------------------------------
    # get triadic CE of 26-edo rotated diatonic scale
    fifth = 692
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="26-EDO diatonic scale")


######################################################
# PORCUPINE[7] TRANSPOSED
######################################################

if porcupine7_tests:
    # ----------------------------------------------------
    # get CE of 15-EDO porcupine[7], rotated
    fifth = 160
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="15-EDO porcupine[7]")

    # ----------------------------------------------------
    # get CE of 37-EDO porcupine[7], rotated
    fifth = 162
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="37-EDO porcupine[7]")

    # ----------------------------------------------------
    # get CE of 59-EDO porcupine[7], rotated
    fifth = 163
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="59-EDO porcupine[7]")

    # ----------------------------------------------------
    # get CE of 22-EDO porcupine[7], rotated
    fifth = 164
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="22-EDO porcupine[7]")

    # ----------------------------------------------------
    # get CE of 29-EDO porcupine[7], rotated
    fifth = 166
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="29-EDO porcupine[7]")


######################################################
# PORCUPINE[8] TRANSPOSED
######################################################

if porcupine8_tests:
    # ----------------------------------------------------
    # get CE of 15-EDO porcupine[8], rotated
    fifth = 160
    scale = sorted(fifth*np.r_[-1:7] % 1200)
    probs = np.ones(8)/8
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="15-EDO porcupine[8]")

    # ----------------------------------------------------
    # get CE of 37-EDO porcupine[8], rotated
    fifth = 162
    scale = sorted(fifth*np.r_[-1:7] % 1200)
    probs = np.ones(8)/8
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="37-EDO porcupine[8]")

    # ----------------------------------------------------
    # get CE of 59-EDO porcupine[8], rotated
    fifth = 163
    scale = sorted(fifth*np.r_[-1:7] % 1200)
    probs = np.ones(8)/8
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="59-EDO porcupine[8]")

    # ----------------------------------------------------
    # get CE of 22-EDO porcupine[8], rotated
    fifth = 164
    scale = sorted(fifth*np.r_[-1:7] % 1200)
    probs = np.ones(8)/8
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="22-EDO porcupine[8]")

    # ----------------------------------------------------
    # get CE of 29-EDO porcupine[8], rotated
    fifth = 166
    scale = sorted(fifth*np.r_[-1:7] % 1200)
    probs = np.ones(8)/8
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="29-EDO porcupine[8]")

######################################################
# MAVILA[7] TRANSPOSED
######################################################
if mavila7_tests:
    # ----------------------------------------------------
    # get triadic CE of 16-edo rotated mavila[7] scale
    fifth = 675
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="16-EDO mavila[7] Scale")

    # ----------------------------------------------------
    # get triadic CE of 9-edo rotated mavila[7] scale
    fifth = 667
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="9-EDO mavila[7] Scale")

    # ----------------------------------------------------
    # get triadic CE of 23-edo rotated mavila[7] scale
    fifth = 678
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="23-EDO mavila[7] Scale")

    # ----------------------------------------------------
    # get triadic CE of 25-edo rotated mavila[7] scale
    fifth = 672
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="25-EDO mavila[7] Scale")

    # ----------------------------------------------------
    # get triadic CE of 43-edo rotated mavila[7] scale
    fifth = 670
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=3, transpose=True, incr=INCR_ALL,
          title="43-EDO mavila[7] Scale")
