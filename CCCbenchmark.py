#!/usr/bin/env python3
"""
This file contains tests and demonstrations of various types of categorical
mutual information. In particular, this file tests the categorical mutual
information of MOS's.

The global value S_ALL sets the value of s for all demos (makes it easy to
change them all at once).
"""

import numpy as np
from CEcore import CCCFromScale
from timeit import default_timer as timer
import time
import sys

def writenow(str):
    sys.stderr.flush()
    sys.stdout.flush()
    sys.stdout.write(str + "\n")
    sys.stderr.flush()
    sys.stdout.flush()

scl = np.sort((696*np.r_[0:7]) % 1200)

# below are original methods - NM and TNC way too slow, remove stuff requiring
# jacobian
# methods = ["Nelder-Mead",  "Powell",  "CG",  "BFGS",  "Newton-CG",  "L-BFGS-B",
#            "TNC",  "COBYLA",  "SLSQP",  "trust-constr", "dogleg",  "trust-ncg",
#            "trust-exact",  "trust-krylov"]

methods = ["SLSQP", "CG",  "BFGS",  "L-BFGS-B",
           "COBYLA",  "trust-constr"]

for m in methods:
    writenow("\n\n=== %s ===\n" % m)
    try:
        t_start = timer()
        for n in range(0, 10):
            c1 = CCCFromScale(scl, incr=2, s=10, n=2, a=1.001, tol=1e-7, transpose=True, method=m)
            c2 = CCCFromScale(scl, incr=2, s=10, n=2, a=2, tol=1e-7, transpose=True, method=m)
        t_end = timer()
        writenow("!!! %s elapsed time: %f - c1['CCC'] = %f, c2['CCC'] = %f" % (m, t_end-t_start, c1['CCC'], c2['CCC']))
        time.sleep(0.2)
    except Exception as e:
        writenow("Oh no! %s - %s" % (m, str(e)))
