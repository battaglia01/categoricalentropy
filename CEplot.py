#!/usr/bin/env python3

from CEcore import CEFromScale, ChordsFromScale, \
                   CMIFromMOSGenerators, CCCFromMOSGenerators, CMIFromEDOs
import numpy as np
from math import floor
import matplotlib as mpl
import matplotlib.pyplot as plt
import mplcursors
import os
import pickle
import getpass
import time
import pdb
import itertools
from scipy.signal import find_peaks


plt.interactive(True)

########################################################################
# INTERNAL HELPER FUNCTIONS
########################################################################

def __get_s_str(s, n, transpose):
    """
    Gets the total and raw `s`, formatted as a string
    :param s: the "total" s (raw is geneated from this)
    :param n: the number of notes
    :param transpose: is this transpositionally invariant?
    :return: the formatted string
    """
    if s is not None:
        s_str = ("total s=%.1f¢ (%.1f¢ raw)" % (s, s/(n**(1/(2*(n-1)))))
                 if transpose else "s=%.1f¢" % s)
    else:
        s_str = ""
    return s_str


def __get_title_str(ent_type, n, transpose, title, incr=None):
    """
    Gets the title string for the entire set of parameters.
    Reusable so we can easily change for multiple functions.
    :param ent_type: entropy type we're calculating ("CE", "CMI", or "CCC")
    :param n: number of notes
    :param a: value of the Renyi parameter a
    :param transpose: True if this is transpositionally-invariant
    :param title: name of the scale
    :param incr: optional. step between adjacent cents on the x-axis (or None)
    :return: the formatted string
    """
    trans_str = "Transposition-Invariant" if transpose else "Non-Transposed"
    incr_str = ", incr=%.1f¢" % incr if incr is not None else ""
    n_switch = {1: "Monadic ", 2: "Dyadic ", 3: "Triadic ", 4: "Tetradic "}
    n_str = n_switch.get(n, str(n)+"-")
    return 'exp(%s%s), %s%s\n%s' \
           % (n_str, ent_type, trans_str, incr_str, title)


def __filename_str(ent_type, s, n, a, transpose, title):
    """
    Returns a formatted filename string for the current set of parameters.
    Also includes the username.
    :param ent_type: entropy type we're calculating ("CE", "CMI", or "CCC")
    :param s: the standard deviation
    :param n: number of notes
    :param a: value of the Renyi parameter a
    :param transpose: True if this is transpositionally-invariant
    :param title: name fo the scale
    :return: the formatted string
    """
    trans_str = "T" if transpose else ""
    s_str = (str(s)+"c")
    valid = \
        "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM-[]()"
    title = "".join([t if t in valid else "_" for t in title])
    username = getpass.getuser()
    return '%d%s-%s_%s_s=%s_a=%s--%s' \
           % (n, trans_str, ent_type, title, s_str, str(a), username)


def __good_legend(*args, **kwargs):
    """
    Checks if a legend exists, removes it before creating a new one.
    """
    if plt.gcf().legends:
        for l in plt.gcf().legends:
            l.remove()
    plt.figlegend(*args, **kwargs)


def __draw_everything():
    """
    Flushes the current plt buffer, draws, and pauses for 0.01 sec.
    """
    plt.gcf().canvas.draw()
    plt.gcf().canvas.flush_events()
    plt.pause(0.01)


def __plot1D(x, y, label="_", title=None, equiv=1200, xtickres=None):
    """
    Simple function to plot a lineseries and auto-adjust ticks, grid, cursors,
    etc. Reused in several functions below.
    :param x: the x-axis, whether that represents, cents, generators, etc.
    :param y: values to plot on the y-axis.
    :param label: the label to give to this plot on the legend.
    :param title: the title to put on the plot.
    :param equiv: the equivalence interval (1200 by default).
    :param xtickres: resolution on x-axis tick labels
    """

    # create plot
    newplot = plt.plot(x, y, label=label, linewidth=1)

    # if the user didn't give an explicit tick resolution, get one
    if xtickres is None:
        xtickres = 10**np.floor(np.log10(np.ptp(x)))
        xtickres = min(xtickres, 100)
        xtickres = (equiv/xtickres + 1)*1j
    else:
        xtickres = 10**np.round(np.log10(xtickres))  # round to power of 10

    # auto-configure plot settings, cursors
    plt.xticks(np.r_[0:equiv:xtickres])
    plt.minorticks_on()
    plt.grid(True, which='major')
    plt.grid(True, which='minor', linestyle='--', alpha=0.5)
    plt.xlim([min(x), max(x)])
    # plt.autoscale(axis='x', tight=True) # not sure why this is needed

    # add cursors
    mplcursors.cursor(newplot, hover=True)

    # title
    plt.title(title)


def __plot2D(x, y, z, label="_", title=None, equiv=1200, plottype='imshow',
             projection='2d'):
    """
    Simple function to do a 2D tricontour plot, given a set of vectors x, y, z.
    Auto-adjusts ticks, grid, cursors, etc. Reused in several functions below.

    :param x: x axis (so cents, for instance).
    :param y: y axis (so also cents, for instance)
    :param z: z values (so the CE).
    :param label: the label to give to this plot on the legend.
    :param title: the title to put on the plot.
    :param equiv: the equivalence interval (1200 by default).
    """

    # Create axes, either 3D or 2D.
    if projection=='3d':
        ax = plt.gca(projection='3d', proj_type='ortho')
    else:
        ax = plt.gca()

    # imshow is *much* faster than tricontourf!
    if plottype == 'imshow':
        # convert array to square
        im = np.reshape(z, (len(set(x)), len(set(y))), order='C')

        # get equal-quantile boundaries for contourization
        levels = 10
        bounds = np.quantile(im, np.r_[0:1:(levels+1)*1j])
        regular = np.r_[np.min(im):np.max(im):(levels+1)*1j]
        bounds = np.sqrt(0.5*bounds**2 + 0.5*regular**2)
        #bounds = regular
        # set up the colormap using the default "viridis" map
        cmap = mpl.colors.Colormap("viridis")
        contours = mpl.colors.BoundaryNorm(bounds, cmap.N)
        
        newplot = ax.imshow(im, interpolation="bilinear", filterrad=1,
                            origin='lower', norm=contours,
                            extent=(min(x), max(x), min(y), max(y)))
        plt.gca().set_label(label)
    elif plottype == 'tricontourf':
        newplot = ax.tricontourf(x, y, z, antialiased=True, label=label, levels=50)


    # change ticks, labels, axes, and grids
    plt.xticks(np.r_[0:equiv:13j])
    plt.yticks(np.r_[0:equiv:13j])
    plt.minorticks_on()
    plt.grid(True, which='major', alpha=3/16)
    plt.grid(True, which='minor', linestyle='--', alpha=1/8)
    ax.set_aspect('equal', 'box')
    plt.autoscale(axis='both', tight=True)

    # add colorbar
    cbar = plt.colorbar(newplot)

    # add cursors
    mplcursors.cursor(newplot, hover=True)

    # title
    plt.title(title)


def __write_obj_to_file(obj, filename):
    """
    Stores the object in a pickle file.
    :param obj: the object to store
    :param filename: the filename (without extension).
    :return:
    """
    if not os.path.isdir("pickles"):
        os.mkdir("pickles")
    with open("pickles/"+filename+".pickle", "wb") as f:
        pickle.dump(obj, f)


########################################################################
# MAIN FUNCTIONS - GET CE/CMI/CCC, WRITE, PLOT
########################################################################


# compute the CE of a probability-weighted scale.
def do_CE(scl, prob, s, n, transpose, a=1, incr=1, title="", new_figure=True,
          equiv=1200, xtickres=None):
    # assert visual dimension is no higher than 2
    dim = n if not transpose else n - 1
    assert dim == 1 or dim == 2, "ERROR: visual dimension must be 1 or 2"

    # if they send in multiple values of s or a, call recursively for each
    if type(s) is list or type(a) is list:
        assert dim == 1, \
               "ERROR: if more than one value of s or a, dim must be 1"
        # put lists into singleton
        s = [s] if type(s) is not list else s
        a = [a] if type(a) is not list else a
        # only make a new figure the first time
        new_figure = True
        peakstrs = []
        for (s_val, a_val) in itertools.product(s, a):
            result_obj = do_CE(scl, prob, s_val, n, transpose, a_val, incr,
                               title, new_figure, equiv)
            new_figure = False
            peakstrs += [write_peaks(result_obj)]
        # print the peaks and quit
        print("\n\n")
        print("==== Maxima Information ====")
        for p in peakstrs:
            print(p)
        return

    # compute CE and get scatter plot of scale degrees
    x_incr = (equiv/incr + 1)*1j
    cents = np.r_[0:equiv:x_incr]
    cents = np.array(list(itertools.product(cents,repeat=dim)))
    results = CEFromScale(cents, scl, prob, s, n, a, transpose, equiv=equiv)

    highlights_x, _ = ChordsFromScale(scl, prob, n, transpose, equiv=equiv)
    highlights_y = CEFromScale(highlights_x, scl, prob, s, n, a, transpose,
                               equiv=equiv)

    result_obj = {"type": "CE",
                  "params": {"scl": scl,
                             "prob": prob,
                             "s": s,
                             "n": n,
                             "transpose": transpose,
                             "a": a,
                             "incr": incr,
                             "title": title,
                             "equiv": equiv,
                             "xtickres": xtickres},
                  "data": {"cents": cents,
                           "results": results,
                           "highlights_x": highlights_x,
                           "highlights_y": highlights_y}}
    __write_obj_to_file(result_obj,
                        __filename_str("CE", s, n, a, transpose, title))
    plot_CE(result_obj, new_figure)
    return result_obj


def do_CMI_EDOs(EDOs, s, n, transpose, prob=None, a=1, incr=1, tol=1e-10,
                sparse=False, title="", new_figure=True, equiv=1200,
                xtickres=None):

    # if they send in multiple values of s or a, call again for each
    if type(s) is list or type(a) is list:
        # put lists into singleton
        s = [s] if type(s) is not list else s
        a = [a] if type(a) is not list else a
        # only make a new figure the first time
        new_figure = True
        peakstrs = []
        for (s_val, a_val) in itertools.product(s, a):
            result_obj = do_CMI_EDOs(EDOs, s_val, n, transpose, prob, a_val,
                                     incr, tol, sparse, title, new_figure,
                                     equiv=equiv, xtickres=xtickres)
            new_figure = False
            peakstrs += [write_peaks(result_obj)]
        # print the peaks and quit
        print("\n\n")
        print("==== Maxima Information ====")
        for p in peakstrs:
            print(p)
        return

    results = CMIFromEDOs(EDOs, prob, s, n, transpose, incr, a, tol, sparse,
                          equiv=equiv)

    result_obj = {"type": "CMI",
                  "params": {"EDOs": EDOs,
                             "s": s,
                             "n": n,
                             "transpose": transpose,
                             "prob": prob,
                             "a": a,
                             "incr": incr,
                             "tol": tol,
                             "sparse": sparse,
                             "title": title,
                             "equiv": equiv,
                             "xtickres": xtickres},
                  "data": {"results": results}}
    __write_obj_to_file(result_obj,
                        __filename_str("CMI", s, n, a, transpose, title))
    plot_CMI(result_obj, new_figure)
    plt.xticks(np.r_[min(EDOs):max(EDOs)+1:2])
    plt.xlim([min(EDOs), max(EDOs)])
    return result_obj


def do_CMI_gens(gens, notes, s, n, transpose, prob=None, a=1, incr=1, per=1,
                tol=1e-10, sparse=False, title="", at_most=False,
                new_figure=True, equiv=1200, xtickres=None):

    # if they send in multiple values of s or a, call again for each
    if type(s) is list or type(a) is list:
        # put lists into singleton
        s = [s] if type(s) is not list else s
        a = [a] if type(a) is not list else a
        # only make a new figure the first time
        new_figure = True
        peakstrs = []
        for (s_val, a_val) in itertools.product(s, a):
            result_obj = do_CMI_gens(gens, notes, s_val, n, transpose, prob,
                                     a_val, incr, per, tol, sparse, title,
                                     at_most, new_figure, equiv=equiv,
                                     xtickres=xtickres)
            new_figure = False
            peakstrs += [write_peaks(result_obj)]
        # print the peaks and quit
        print("\n\n")
        print("==== Maxima Information ====")
        for p in peakstrs:
            print(p)
        return

    results = CMIFromMOSGenerators(gens, notes, per, prob, s, n, transpose,
                                   incr, a, tol, sparse, at_most, equiv=equiv)
    result_obj = {"type": "CMI",
                  "params": {"gens": gens,
                             "notes": notes,
                             "per": per,
                             "s": s,
                             "n": n,
                             "transpose": transpose,
                             "prob": prob,
                             "a": a,
                             "incr": incr,
                             "tol": tol,
                             "sparse": sparse,
                             "title": title,
                             "at_most": at_most,
                             "equiv": equiv,
                             "xtickres": xtickres},
                  "data": {"results": results}}
    __write_obj_to_file(result_obj,
                        __filename_str("CMI", s, n, a, transpose, title))
    plot_CMI(result_obj, new_figure)
    return result_obj


def do_CCC_gens(gens, notes, s, n, transpose, a=1, incr=1, per=1, tol=1e-10,
                sparse=False, title="", at_most=False, new_figure=True,
                max_iter=100, equiv=1200, xtickres=None):

    # if they send in multiple values of s or a, call again for each
    if type(s) is list or type(a) is list:
        # put lists into singleton
        s = [s] if type(s) is not list else s
        a = [a] if type(a) is not list else a
        # only make a new figure the first time
        new_figure = True
        peakstrs = []
        for (s_val, a_val) in itertools.product(s, a):
            result_obj = do_CCC_gens(gens, notes, s_val, n, transpose, a_val,
                                     incr, per, tol, sparse, title, at_most,
                                     new_figure, max_iter, equiv=equiv,
                                     xtickres=xtickres)
            new_figure = False
            peakstrs += [write_peaks(result_obj)]
        # print the peaks and quit
        print("\n\n")
        print("==== Maxima Information ====")
        for p in peakstrs:
            print(p)
        return

    # compute CCC of generators
    bestprobs, results = CCCFromMOSGenerators(gens, notes, per, s, n,
                                              transpose, incr, a, tol, sparse,
                                              at_most, max_iter, equiv=equiv)
    result_obj = {"type": "CCC",
                  "params": {"gens": gens,
                             "notes": notes,
                             "per": per,
                             "s": s,
                             "n": n,
                             "transpose": transpose,
                             "a": a,
                             "incr": incr,
                             "tol": tol,
                             "title": title,
                             "at_most": at_most,
                             "max_iter": max_iter,
                             "equiv": equiv,
                             "xtickres": xtickres},
                  "data": {"bestprobs": bestprobs,
                           "results": results}}
    __write_obj_to_file(result_obj,
                        __filename_str("CCC", s, n, a, transpose, title))
    plot_CCC(result_obj, new_figure)
    return result_obj


########################################################################
# PLOT FUNCTIONS - PLOT OBJ FROM "get" functions
########################################################################

def plot_CE(obj, new_figure=True):
    s = obj['params']['s']
    n = obj['params']['n']
    transpose = obj['params']['transpose']
    a = obj['params']['a']
    title = obj['params']['title']
    xtickres = obj['params']['xtickres']

    cents = obj['data']['cents']
    results = obj['data']['results']
    highlights_x = obj['data']['highlights_x']
    highlights_y = obj['data']['highlights_y']

    to_plot = 2**results
    highlights_y_to_plot = 2**highlights_y

    dim = n if not transpose else n-1

    if new_figure:
        plt.figure()

    s_str = __get_s_str(s, n, transpose)
    if dim == 1:
        __plot1D(cents, to_plot, xtickres=xtickres,
                 title=__get_title_str("CE", n, transpose, title))
        plt.scatter(highlights_x, highlights_y_to_plot,
                    label="%s, a=%s" % (s_str, str(a)))

    # if dim = 2: trisurf plot CE and do scatter
    if dim == 2:
        __plot2D(cents[:, 0], cents[:, 1], to_plot,
                 title=__get_title_str("CE", n, transpose, title))
        plt.scatter(highlights_x[:, 0], highlights_x[:, 1],
                    label="Reference scale, %s, a=%s" % (s_str, str(a)), c="red",
                    s=2, marker="x")

    # add legend here because if you call this more than once, it adds
    # multiple legends
    __good_legend(loc="upper right")
    __draw_everything()
    plt.show()


def plot_CMI(obj, new_figure=True):
    s = obj['params']['s']
    n = obj['params']['n']
    incr = obj['params']['incr']
    transpose = obj['params']['transpose']
    a = obj['params']['a']
    title = obj['params']['title']
    xtickres = obj['params']['xtickres']

    if 'gens' in obj['params']:
        x = obj['params']['gens']
    else:
        x = obj['params']['EDOs']

    results = obj['data']['results']

    dim = n-1 if transpose else n

    to_plot = 2 ** (results / dim)

    # since this is rank-2, can be 1D only
    if new_figure:
        plt.figure()

    s_str = __get_s_str(s, n, transpose)
    __plot1D(x, to_plot, label="CMI, %s, a=%s" % (s_str, str(a)),
             title=__get_title_str("CMI", n, transpose, title, incr),
             xtickres=xtickres)

    # add legend here because if you call this more than once, it adds
    # multiple legends
    __good_legend(loc="upper right")
    __draw_everything()
    plt.show()


def plot_CCC(obj, new_figure=True):
    s = obj['params']['s']
    n = obj['params']['n']
    incr = obj['params']['incr']
    transpose = obj['params']['transpose']
    a = obj['params']['a']
    title = obj['params']['title']
    xtickres = obj['params']['xtickres']

    gens = obj['params']['gens']
    results = obj['data']['results']

    dim = n-1 if transpose else n

    to_plot = 2 ** (results / dim)

    # since this is rank-2, can be 1D only
    if new_figure:
        plt.figure()

    s_str = __get_s_str(s, n, transpose)
    __plot1D(gens, to_plot, label="CCC, %s, a=%s" % (s_str, str(a)),
             title=__get_title_str("CCC", n, transpose, title, incr),
             xtickres=xtickres)

    # add legend here because if you call this more than once, it adds
    # multiple legends
    __good_legend(loc="upper right")
    __draw_everything()
    plt.show()


def plot_from_files(file, new_figure=True):
    # recurse for individual files
    if type(file) is list:
        new_figure = True
        objs = []
        for f in file:
            objs += [plot_from_files(f, new_figure)]
            new_figure = False
        return objs

    # if we've made it this far, this is an individual file
    # read file and plot accordingly
    with open(file, "rb") as f:
        obj = pickle.load(f)
        if obj['type'] == "CE":
            plot_CE(obj, new_figure)
        elif obj['type'] == "CMI":
            plot_CMI(obj, new_figure)
        elif obj['type'] == "CCC":
            plot_CCC(obj, new_figure)

    write_peaks(obj)
    return obj


########################################################################
# ANALYSIS FUNCTIONS - PEAK FINDING AND ETC
########################################################################

def write_peaks(objs, threshold=0, prominence=0.001):
    # promote to singleton list if necessary
    objs = [objs] if type(objs) is not list else objs
    peakstrs = []
    for obj in objs:
        # Check the parameters to see if the x-axis is an interval in cents,
        # an EDO number, a generator size...
        res = obj['data']['results']
        peaks, _ = find_peaks(res, threshold=threshold, prominence=prominence)
        if 'cents' in obj['data']:
            inds = obj['data']['cents'][peaks]
        elif 'EDOs' in obj['params']:
            inds = obj['params']['EDOs'][peaks]
        elif 'gens' in obj['params']:
            inds = obj['params']['gens'][peaks]

        # Now get each peak, write it to the command line, and add it to
        # the "peakstr" that we're going to return
        s = obj['params']['s']
        a = obj['params']['a']
        tr = obj['params']['transpose']
        title = obj['params']['title']
        peakstr = 'Maxima for %s (s=%g, a=%g, transpose=%s):\n' % (title, s, a, tr)
        for p in range(len(peaks)):
            peakstr += '%.1f: %.4f\n' % (inds[p], res[p])
        print(peakstr)
        peakstrs += [peakstr]

    # if only one object, don't return an array
    if len(peakstrs) == 1:
        peakstrs = peakstrs[0]

    return peakstrs
