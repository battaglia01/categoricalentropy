#!/usr/bin/env python3
"""
This file contains tests and demonstrations of various types of categorical
entropy.

In particular, this file tests "1D" categorical entropy:
- Monadic CE
- Dyadic CE with transpositional-invariance

The global value S_ALL sets the value of s for all demos (makes it easy to
change them all at once).
"""

from CEplot import do_CE
import numpy as np

# GLOBALS
S_ALL = [15, 20, 25]    # value of s, global so we can change all easily

# Tests to do
monadic_tests = False
diatonic_tests = True
porcupine7_tests = False
porcupine8_tests = False
mavila7_tests = False

######################################################
# BASIC MONADIC NON-TRANSPOSED
######################################################
if monadic_tests:
    # ----------------------------------------------------
    # get monadic CE of 12-EDO, non-transposed
    scale = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100]
    probs = np.ones(12)/12
    do_CE(scale, probs, s=S_ALL, n=1, transpose=False,
          title='12-EDO')

    # ----------------------------------------------------
    # get monadic CE of slendro, non-transposed
    scale = [0, 228, 484, 728, 960]
    probs = np.ones(5)/5
    do_CE(scale, probs, s=S_ALL, n=1, transpose=False,
          title='Observed Javanese Slendro, Helmholtz/Ellis p. 518, nr. 94')

    # ----------------------------------------------------
    # get monadic CE of 12-edo diatonic scale, non-transposed
    fifth = 700
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=1, transpose=False,
          title='12-EDO diatonic scale')

    # ----------------------------------------------------
    # get monadic CE of 719-cent generated diatonic scale, non-transposed
    fifth = 715
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=1, transpose=False,
          title='Diatonic scale, ~28\\47 Generator')

    # ----------------------------------------------------
    # get monadic CE of 687-cent generated diatonic scale, non-transposed
    fifth = 687
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=1, transpose=False,
          title='Diatonic scale, 23\\40 Generator')

######################################################
# DIATONIC TRANSPOSED
######################################################
if diatonic_tests:
    # ----------------------------------------------------
    # get dyadic CE of 12-edo transposed diatonic scale
    fifth = 700
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="12-EDO diatonic scale")

    # ----------------------------------------------------
    # get dyadic CE of 31-edo transposed diatonic scale
    fifth = 697
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="31-EDO diatonic scale")

    # ----------------------------------------------------
    # get dyadic CE of 19-edo transposed diatonic scale
    fifth = 695
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="19-EDO diatonic scale")

    # ----------------------------------------------------
    # get dyadic CE of 17-edo transposed diatonic scale
    fifth = 706
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="17-EDO diatonic scale")

    # ----------------------------------------------------
    # get dyadic CE of 26-edo transposed diatonic scale
    fifth = 692
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="26-EDO diatonic scale")

######################################################
# PORCUPINE[7] TRANSPOSED
######################################################

if porcupine7_tests:
    # ----------------------------------------------------
    # get CE of 15-EDO porcupine[7], transposed
    fifth = 160
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="15-EDO porcupine[7]")

    # ----------------------------------------------------
    # get CE of 37-EDO porcupine[7], transposed
    fifth = 162
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="37-EDO porcupine[7]")

    # ----------------------------------------------------
    # get CE of 59-EDO porcupine[7], transposed
    fifth = 163
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="59-EDO porcupine[7]")

    # ----------------------------------------------------
    # get CE of 22-EDO porcupine[7], transposed
    fifth = 164
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="22-EDO porcupine[7]")

    # ----------------------------------------------------
    # get CE of 29-EDO porcupine[7], transposed
    fifth = 166
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="29-EDO porcupine[7]")

######################################################
# PORCUPINE[8] TRANSPOSED
######################################################

if porcupine8_tests:
    # ----------------------------------------------------
    # get CE of 15-EDO porcupine[8], transposed
    fifth = 160
    scale = sorted(fifth*np.r_[-1:7] % 1200)
    probs = np.ones(8)/8
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="15-EDO porcupine[8]")

    # ----------------------------------------------------
    # get CE of 37-EDO porcupine[8], transposed
    fifth = 162
    scale = sorted(fifth*np.r_[-1:7] % 1200)
    probs = np.ones(8)/8
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="37-EDO porcupine[8]")

    # ----------------------------------------------------
    # get CE of 59-EDO porcupine[8], transposed
    fifth = 163
    scale = sorted(fifth*np.r_[-1:7] % 1200)
    probs = np.ones(8)/8
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="59-EDO porcupine[8]")

    # ----------------------------------------------------
    # get CE of 22-EDO porcupine[8], transposed
    fifth = 164
    scale = sorted(fifth*np.r_[-1:7] % 1200)
    probs = np.ones(8)/8
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="22-EDO porcupine[8]")

    # ----------------------------------------------------
    # get CE of 29-EDO porcupine[8], transposed
    fifth = 166
    scale = sorted(fifth*np.r_[-1:7] % 1200)
    probs = np.ones(8)/8
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="29-EDO porcupine[8]")


######################################################
# MAVILA[7] TRANSPOSED
######################################################
if mavila7_tests:
    # ----------------------------------------------------
    # get triadic CE of 16-edo transposed mavila[7] scale
    fifth = 675
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="16-EDO mavila[7] scale")

    # ----------------------------------------------------
    # get triadic CE of 9-edo transposed mavila[7] scale
    fifth = 667
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="9-EDO mavila[7] scale")

    # ----------------------------------------------------
    # get triadic CE of 23-edo transposed mavila[7] scale
    fifth = 678
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="23-EDO mavila[7] scale")

    # ----------------------------------------------------
    # get triadic CE of 25-edo transposed mavila[7] scale
    fifth = 672
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True,
          title="25-EDO mavila[7] scale")
