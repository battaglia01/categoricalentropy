#!/usr/bin/env python3
"""
This file contains tests and demonstrations of various types of categorical
Renyi Entropy.

The global value S_ALL sets the value of s for most demos (makes it easy to
change them all at once). Likewise with A_ALL.

When the global S_ALL or A_ALL isn't being used for a particular demo, this is
indicated in the comments.
"""

from CEplot import do_CMI_gens, do_CMI_EDOs, do_CE
import numpy as np

# GLOBALS
S_ALL = [10, 12.5, 15, 17.5, 20]    # global so we can change all easily
A_ALL = [1, 2, 3, 4, 5, 6]

# Tests to do
CMI_tests = False
CE_tests = False

######################################################
# RENYI-CMI TESTS - S CONSTANT, VARIOUS A
######################################################

if CMI_tests:

    # ----------------------------------------------------
    # get CMI of monadic EDOs, non-transposed
    EDOs = np.r_[1:50]  # 0 to 600
    do_CMI_EDOs(EDOs, s=S_ALL, n=1, a=A_ALL, transpose=False, incr=1,
                tol=1e-10, sparse=False, xtickres=1,
                title="EDO spectrum")

    # ----------------------------------------------------
    # get CMI of diatonic MOS spectrum, transposed
    # NOTE - only test s=15
    gens = np.r_[686:720:341j]  # 7 to 12-EDO. 341j means 0.1 cent incr
    do_CMI_gens(gens, notes=7, s=15, n=2, a=A_ALL, transpose=True,
                incr=0.1, tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Diatonic MOS spectrum, s=15 cents")

    # ----------------------------------------------------
    # get CMI of chromatic MOS spectrum, transposed
    # NOTE - only test s=15
    gens = np.r_[686:720:341j]  # 7 to 12-EDO. 341j means 0.1 cent incr
    do_CMI_gens(gens, notes=12, s=15, n=2, a=A_ALL, transpose=True,
                incr=0.1, tol=1e-10, sparse=False, at_most=False, xtickres=1,
                title="Chromatic MOS spectrum, s=15 cents")

    # ----------------------------------------------------
    # get at-most-decatonic CMI, transposed
    gens = np.r_[0:600:601j]  # 0 to 600
    do_CMI_gens(gens, notes=10, s=S_ALL, n=2, a=A_ALL, transpose=True,
                incr=1, tol=1e-10, sparse=False, at_most=True, xtickres=100,
                title="At-most-decatonic MOS spectrum")


######################################################
# RENYI-CE TESTS - S CONSTANT, VARIOUS A
######################################################
if CE_tests:
    # ----------------------------------------------------
    # get monadic CE of 12-EDO, non-transposed
    scale = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100]
    probs = np.ones(12)/12
    do_CE(scale, probs, s=S_ALL, n=1, transpose=False, a=A_ALL,
          title='12-EDO')

    # ----------------------------------------------------
    # get monadic CE of 24-EDO, non-transposed
    scale = [x for x in range(0, 1200, 50)]
    probs = np.ones(24)/24
    do_CE(scale, probs, s=S_ALL, n=1, transpose=False, a=A_ALL,
          title='24-EDO')

    # ----------------------------------------------------
    # get dyadic CE of 12-edo transposed diatonic scale
    fifth = 700
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True, a=A_ALL,
          title="12-EDO diatonic scale")

    # ----------------------------------------------------
    # get CE of 15-EDO porcupine[7], transposed
    fifth = 160
    scale = sorted(fifth*np.r_[-1:6] % 1200)
    probs = np.ones(7)/7
    do_CE(scale, probs, s=S_ALL, n=2, transpose=True, a=A_ALL,
          title="15-EDO porcupine[7]")
